# Propagation Path List File Format

The **propagation path list** file is designed as an open exchange format for sound propagation based on sound paths.
Geometric acoustics typically determine these sound paths.
A sound path describes the propagation from a sound source to a receiver, including interaction with a geometry.

[TOC]

## Concept

The data scheme is based on the *JavaScript Object Notation* (`.json`).
It was chosen since it is human-readable and can represent complex tree-like structures.
Furthermore, it is widely used and supported by many programming languages.

In its current form, the top level contains two name-value-pairs:

- **propagation_paths**: `json`-array containing PropagationPath objects.
- **material_database**: `json`-object containing a database with acoustic material specifications used in the geometry.

For example:

```json
{
   "propagation_paths": [ { ... }, { ... } ],
   "material_database": { ... }
}
```

The relationships between the elements can be represented like this:

```mermaid
flowchart TD;
    PropagationPathList --> material_database;
    PropagationPathList --> propagation_paths;

    material_database -->|0...| MaterialBase;

    propagation_paths -->|0...| PropagationPath;

    propagation_path -->|0...| PropagationAnchorBase;

    PropagationAnchorBase --o Source;
    PropagationAnchorBase --o Receiver;
    PropagationAnchorBase --o SpecularReflection;
    PropagationAnchorBase --o EdgeDiffraction;
    PropagationAnchorBase --o Inhomogeneity;
    PropagationAnchorBase --o InhomogeneitySource;
    PropagationAnchorBase --o InhomogeneityReceiver;
    PropagationAnchorBase --o InhomogeneitySpecularReflection;

    SpecularReflection <--> MaterialBase;
```

![Data scheme](./figs/ppl_schema.svg)

## Material database

The `material_database`-object contains name-value-pairs where the name is the material's name and the value of an object containing information about its acoustic properties.

An example could look like this:

```json
{
    ...
    "material_database": {
        "concrete": { ... },          // Material with the name "concrete"
        "acoustic_ceiling": { ... },  // Material with the name "acoustic_ceiling"
        ...
    }
    ...
}
```

The `material`-object contains the properties of the material needed for acoustic modeling.
At the moment, it is primarily used for specular reflections.
Each material has a _type_ member that specifies the type of the material.
Two types are currently supported:

- `ScalarMaterial`: a material with scalar absorption and scattering values.
- `ThirdOctaveMaterial`: a material with third-octave absorption, scattering, impedance_real and impedance_imag values.

An example of a third-octave `material`-object could look like this:

```json
{
    "type": "ThirdOctaveMaterial",
    "absorption": [ 0.1, 0.2, ... ],
    "scattering": [ ... ]
}
```

### Scalar material

The scalar material contains scalar floating point values for *absorption* and *scattering*.

```json
{
    ...
    "absorption" : 0.1,
    "scattering" : 0.1
}
```

### Third-octave material

In the case of a third-octave material, the properties are given as arrays with third-octave values for *absorption*, *scatter, *impedance_real* and *impedance_imag*.
Note, that the length of the arrays must be 31 and that the impedance values are optional.

```json
{
    ...
    "absorption" : [ 0.1, 0.2, ... ],
    "scattering" : [ ... ],
    "impedance_real" : [ ... ],
    "impedance_imag" : [ ... ]
}
```

## Propagation Path

The value of the top-level *`propagation_paths`* key is a `json`-array containing `propagation_path`-objects.
Each of the `propagation_path`-objects describes one possible path from a source to a receiver.
This could include the direct path or a path that is reflected on a surface and then diffracted around an edge.

Each `propagation_path`-object has an optional *identifier* wich can be used to identify the path with a name and *propagation_anchors*, which is an array of `propagation_anchor`-objects.

```json
{
    "identifier": "",
    "propagation_anchors": [ { ... }, { ... } ]
}
```

The `propagation_anchor`-object describes an interaction of the sound path with the scene.
For example, the "start" of the sound path at the source is one such interaction.
As a result, different types of anchor points exist.
These are:

- **Source**: the sound source
- **Receiver**:  the sound receiver
- **SpecularReflection**: a specular reflection on a surface
- **EdgeDiffraction**: a diffraction on an edge
- **Inhomogeneity**: a point in an inhomogeneous medium
- **InhomogeneitySource**: a source in an inhomogeneous medium
- **InhomogeneityReceiver**: a receiver in an inhomogeneous medium
- **InhomogeneitySpecularReflection**: a specular reflection in an inhomogeneous medium

All `propagation_anchor`-object have some common properties.
The *type*, being one of the above mentioned and an *interaction_point* which describes the 3D point in which the interaction occurs.

```json
{
    "anchor_type": "Source", // or Receiver, ...
    "interaction_point": [ x, y, z ] // in OpenGL Cartesian coordinates
}
```

### `Source`

The `Source` anchor point describes the sound source of the scene.
In addition to the common properties, the sound source can have a *name*, an *orientation* given as a quaternion and an optional *directivity_id* which defines the directivity of the source.
The value of *directivity_id* is a string which defines the name of the directivity.
The `interaction_point` is the position of the sound source.

```json
{
    ...
    "type": "Source",
    "name": "Loudspeaker",
    "directivity_id": "Genelec8020c",
    "orientation": [ x, y, z, w ]
}
```

### `Receiver`

The `Receiver` anchor point describes the sound receiver in the scene.
It follows the same properties as the `Source`, so *name*, *orientation* and *directivity_id*

```json
{
    ...
    "type": "Receiver",
    "name": "Loudspeaker",
    "directivity_id": "Genelec8020c",
    "orientation": [ x, y, z, w ]
}
```

### `SpecularReflection`

The anchor for specular reflections is the `SpecularReflection` anchor.
The `interaction_point` is the point where the reflection occurs.
It specifies an additional *polygon_id* which refers back to the 3D geometry, an optional *face_normal* and an optional *material_id*.
The *face_normal* should be included if it is known.
When a *material_id* is given, it must be contained in the `material_database` with the same name as specified in the value of *material_id*.

```json
{
    ...
    "type": "SpecularReflection",
    "polygon_id": 0,
    "face_normal": [ x, y, z ],
    "material_id": "acoustic_ceiling"
}
```

### `EdgeDiffraction`

If a diffraction on an edge occurs, an `EdgeDiffraction` anchor should be used.
It adds the *main_wedge_face_id* and *opposite_wedge_face_id* properties that refer back to the 3D geometry.

*vertex_start* and *vertex_end* are the 3D points that define the edge on which the diffraction occurs.
Furthermore, the information about the faces that create the wedge on which the diffraction occurs is given.
As such, *main_wedge_face_id* and *main_wedge_face_normal* define the ID and normal of the face "illuminated" by the previous interaction.
While *opposite_wedge_face_id* and *opposite_wedge_face_normal* define the ID and normal of the face not "illuminated" by the previous interaction.

```json
{
    ...
    "type": "EdgeDiffraction",
    "vertex_start": [ x, y, z ],
    "vertex_end": [ x, y, z ],
    "main_wedge_face_id": 0,
    "opposite_wedge_face_id": 1,
    "main_wedge_face_normal": [ x, y, z ],
    "opposite_wedge_face_normal": [ x, y, z ]
}
```

### Anchor types for propagation in inhomogeneous media

In inhomogeneous (moving) media sound paths are usually curved and the speed of sound is not constant.
Thus, the sound path is approximated using a series of points with a time stamp and the propagation direction (wavefront normal) as meta data.
The time stamp usually refers to the time travelled since the sound wave was emitted by the source.
In total, the presented format provides four anchor types.

- `Inhomogeneity`
- `InhomogeneitySource`
- `InhomogeneitySpecularReflection`
- `InhomogeneityReceiver`

All anchor types have in common that they have properties for the wavefront normal and propagation time *t* as discussed above:

```json
{
    ...
    "wavefront_normal": [x y z],
    "time_stamp": t
}
```

The most basic anchor is `Inhomogeneity` which only has the `interaction_point` as additinal property. The other ones extend the respecitve anchor types for homogeneous media, namely [`Source`](#source), [`SpecularReflection`](#specularreflection) and [`Receiver`](#receiver), with the properties above.

Since, the well-known distance law does not hold in such media, the spreading loss must be calculated by other means, e.g. using the Blokhintzev invariant.
Broadly spoken, the spreading loss depends on the density of the sound paths at the receiver.
Since it cannot be derived by a single sound path alone, it must be determined during the sound path simulation process.
Thus, the `inhomogeneity_receiver` anchor has additional property storing the spreading loss factor *g* (usually < 1.0 for source-receiver distances above 1 m).
To avoid numerical issues it is stored in dB meaning 20 log10(*g*).
Here, is an example for a spreading loss factor of 0.5:

```json
{
    ...
    "spreading_loss_db": -6
}
```

## General notes

The coordinate system used in a **propagation path list** file is the OpenGL coordinate system.
This means a right-handed, y-up coordinate system with 1 meter as the unit.

## Examples

### Only direct sound

<details>

<summary markdown="span">Example propagation path list with only direct sound.</summary>

```json
{
   "material_database": {},
   "propagation_paths": [
      {
         "identifier": "",
         "propagation_anchors": [
            {
               "type": "Source",
               "directivity_id": "Loudspeaker_Genelec8020c_1x1",
               "interaction_point": [
                  -2,
                  2,
                  0
               ],
               "name": "Loudspeaker",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            },
            {
               "type": "Receiver",
               "directivity_id": "Omnidirectional",
               "interaction_point": [
                  2,
                  2,
                  0
               ],
               "name": "IdealMicrophone",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            }
         ]
      }
   ]
}
```

</details>

### Direct sound and reflection

<details>

<summary markdown="span">Example propagation path list with direct sound and a floor reflection.</summary>

```json
{
   "material_database": {"mat_Tiles": {
         "absorption": [
            0.00200, 0.00200, 0.00200, 0.00300, 0.00499, 0.00499, 0.00600, 0.00800, 0.00999, 0.01099, 0.00999, 0.01099, 0.01400, 0.01300, 0.01999, 0.02099, 0.02199, 0.02099, 0.02400, 0.02300, 0.02999, 0.02999, 0.02600, 0.02300, 0.02899, 0.03200, 0.02999, 0.03099, 0.02899, 0.03099, 0.03700
         ],
         "impedanceImag": [
            -1039759.125, -883068.9375, -745630.0, -626006.4375, -531668.1875, -451546.5625, -376898.875, -320100.75, -271862.03125, -232254.375, -192722.640625, -163679.625, -138204.875, -116032.2734375, -98546.375, -83208.828125, -69859.3984375, -59331.6875, -50390.48828125, -42060.15625, -35721.75, -30338.53515625, -25616.7109375, -21506.94921875, -18265.8828125, -15334.03125, -12948.66015625, -10997.31640625, -9340.0361328125, -7795.98291015625, -6621.1396484375
         ],
         "impedanceReal": [
            929766.5625, 785848.8125, 660241.625, 551479.375, 466142.28125, 394020.3125, 327171.21875, 276570.0, 233804.75, 198860.25, 164161.71875, 138803.75, 116672.0703125, 97508.421875, 82472.234375, 69349.09375, 57985.86328125, 49070.046875, 41534.9140625, 34550.67578125, 29263.978515625, 24795.966796875, 20896.416015625, 17519.826171875, 14870.486328125, 12486.0361328125, 10556.046875, 8985.1015625, 7657.42919921875, 6426.82373046875, 5495.31982421875
         ],
         "scatter": [
            0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05000, 0.05400, 0.05999, 0.06800, 0.07599, 0.08500, 0.09499, 0.10800, 0.12099, 0.13500, 0.15199, 0.17100, 0.19099, 0.21500, 0.24099, 0.27000, 0.30199, 0.34099, 0.38199
         ],
         "type": "ThirdOctaveMaterial"
      }
   },
   "propagation_paths": [
      {
         "identifier": "",
         "propagation_anchors": [
            {
               "type": "Source",
               "directivity_id": "Loudspeaker_Genelec8020c_1x1",
               "interaction_point": [
                  -2,
                  2,
                  0
               ],
               "name": "Loudspeaker",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            },
            {
               "type": "Receiver",
               "directivity_id": "Omnidirectional",
               "interaction_point": [
                  2,
                  2,
                  0
               ],
               "name": "IdealMicrophone",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            }
         ]
      },
      {
         "identifier": "",
         "propagation_anchors": [
            {
               "type": "Source",
               "directivity_id": "Loudspeaker_Genelec8020c_1x1",
               "interaction_point": [
                  -2,
                  2,
                  0
               ],
               "name": "Loudspeaker",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            },
            {
               "type": "SpecularReflection",
               "face_normal": [
                  0.0,
                  1.0,
                  0.0
               ],
               "interaction_point": [
                  0,
                  0.0,
                  0
               ],
               "material_id": "mat_Tiles",
               "polygon_id": 0
            },
            {
               "type": "Receiver",
               "directivity_id": "Omnidirectional",
               "interaction_point": [
                  2,
                  2,
                  0
               ],
               "name": "IdealMicrophone",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            }
         ]
      }
   ]
}
```

</details>

### One diffraction
<details>

<summary markdown="span">Example propagation path list with a diffraction.</summary>

```json
{
   "material_database": {},
   "propagation_paths": [
      {
         "identifier": "",
         "propagation_anchors": [
            {
               "type": "Source",
               "directivity_id": "Loudspeaker_Genelec8020c_1x1",
               "interaction_point": [
                  -2,
                  2,
                  -1
               ],
               "name": "Loudspeaker",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            },
            {
               "type": "EdgeDiffraction",
               "interaction_point": [
                  0.0,
                  2.0,
                  0.0
               ],
               "main_wedge_face_id": 0,
               "main_wedge_face_normal": [
                  0.0,
                  0.0,
                  -1.0
               ],
               "opposite_wedge_face_id": 1,
               "opposite_wedge_face_normal": [
                  1.0,
                  0.0,
                  0.0
               ],
               "vertex_end": [
                  0,
                  4,
                  0
               ],
               "vertex_start": [
                  0,
                  0.0,
                  0
               ]
            },
            {
               "type": "Receiver",
               "directivity_id": "Omnidirectional",
               "interaction_point": [
                  1,
                  2,
                  2
               ],
               "name": "IdealMicrophone",
               "orientation": [
                  0.0,
                  0.0,
                  0.0,
                  1.0
               ]
            }
         ]
      }
   ]
}
```

</details>
