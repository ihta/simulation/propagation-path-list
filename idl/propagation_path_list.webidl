typedef [Size=3] sequence<double> Vector;
typedef [Size=4] sequence<double> Quaternion;
typedef [Size=31] sequence<double> MagnitudeSpectra;

///
/// \brief Top-level datastructure for the propagation path list
///
/// The propagation path list contains a list of propagation paths, which in turn contain a list of propagation anchors.
/// The propagation anchors can be of type
/// - Source
/// - Receiver
/// - SpecularReflection
/// - EdgeDiffraction
/// - Inhomogeneity
/// - InhomogeneitySource
/// - InhomogeneityReceiver
/// - InhomogeneitySpecularReflection
///
/// The propagation path list also contains a material database, which is a dictionary of materials that can be referenced by the propagation anchors.
/// The material database is optional, and can be used to store materials that are used by the propagation anchors.
/// The material database is a dictionary of materials, where the key is the material identifier and the value is the material itself.
///
dictionary PropagationPathList
{
    ObservableArray<PropagationPath> propagation_paths; ///< List of propagation paths

    record<ByteString, MaterialBase>? material_database; ///< Optional material database storing MaterialBase objects
};

///
/// \brief Container of PropagationAnchors representing a propagation path.
///
dictionary PropagationPath
{
    ///
    /// \brief Identifier of the propagation path.
    ///
    /// This could be the name of the path, or a unique identifier.
    /// For example a path with a source and a receiver might be called "S-R" for source to receiver.
    ///
    string identifier;

    ///
    /// \brief List of propagation anchors
    ///
    /// This list contains the propagation anchors that make up the propagation path.
    ///
    ObservableArray<PropagationAnchorBase> propagation_anchors;
};

///
/// \brief Base class for all propagation anchors
///
dictionary PropagationAnchorBase
{
    ///
    /// \brief Interaction point of the propagation anchor.
    ///
    /// This is the point in space where the propagation anchor interacts with the environment.
    /// For example, for a source this could be the point where the sound is emitted, and for a receiver this could be the point where the sound is received.
    ///
    Vector interaction_point;
};

///
/// \brief Source anchor
///
/// The source anchor is a point in space where sound is emitted.
/// It is assumed to be a point source, i.e. it has no spatial extent.
///
dictionary Source : PropagationAnchorBase
{
    string name; ///< Name of the source

    ///
    /// \brief Identifier of the directivity pattern
    ///
    /// This is the identifier of the directivity pattern that is used by the source.
    /// Currently, the directivity pattern is not part of the data model, but it is assumed that the directivity pattern is stored on disk and is loaded separately.
    ///
    /// \todo The directivies might be added to the data model as well.
    ///
    string directivity_id;

    Quaternion orientation; ///< Orientation of the source, defined as a quaternion
};

///
/// \brief Receiver anchor
///
/// The receiver anchor is a point in space where sound is received.
/// It is assumed to be a point receiver, i.e. it has no spatial extent.
///
dictionary Receiver : PropagationAnchorBase
{
    string name; ///< Name of the source

    ///
    /// \brief Identifier of the directivity pattern
    ///
    /// This is the identifier of the directivity pattern that is used by the receiver.
    /// Currently, the directivity pattern is not part of the data model, but it is assumed that the directivity pattern is stored on disk and is loaded separately.
    ///
    /// \todo The directivies might be added to the data model as well.
    ///
    string directivity_id;

    Quaternion orientation; ///< Orientation of the source, defined as a quaternion
};

///
/// \brief Specular reflection anchor
///
/// The specular reflection anchor is a point in space where sound is reflected from a surface.
/// The reflection is assumed to be specular, i.e. the angle of incidence is equal to the angle of reflection.
///
dictionary SpecularReflection : PropagationAnchorBase
{
    long polygon_id; ///< Identifier of the polygon that is reflected from

    ///
    /// \brief Identifier of the material
    ///
    /// This identifier is used to reference the material in the material database.
    ///
    string material_id;

    Vector face_normal; ///< Normal of the polygon face
};

///
/// \brief Edge diffraction anchor
///
/// The edge diffraction anchor is a point in space where sound is diffracted around an edge.
/// The diffraction is assumed to be caused by a wedge-shaped edge.
///
/// The main wedge face is defined to be the face that is iluminated by the previous propagation anchor.
///
/// \todo: Do we separate outer and inner edge diffraction?
///
dictionary EdgeDiffraction : PropagationAnchorBase
{
    long main_wedge_face_id; ///< Identifier of the main wedge polygon
    long opposite_wedge_face_id; ///< Identifier of the opposite wedge polygon

    Vector vertex_start; ///< Start vertex of the wedges edge
    Vector vertex_end; ///< End vertex of the wedges edge
    Vector main_wedge_face_normal; ///< Normal of the main wedge polygon face
    Vector opposite_wedge_face_normal; ///< Normal of the opposite wedge polygon face
};

///
/// \brief Inhomogeneity anchor
///
/// This serves both as the base class for all inhomogeneity types and as a generic inhomogeneity anchor.
///
dictionary Inhomogeneity : PropagationAnchorBase
{
    Vector wavefront_normal; ///< Normal of the wavefront

    double time_stamp; ///< Time stamp of the inhomogeneity
};

///
/// \brief Inhomogeneity anchor extension for a source
///
dictionary InhomogeneitySource : Source
{
    Vector wavefront_normal; ///< Normal of the wavefront

    double time_stamp; ///< Time stamp of the inhomogeneity
};

///
/// \brief Inhomogeneity anchor extension for a receiver
///
/// Since the distance law does not apply to inhomogenious media, the spreading loss of the path is stored in the inhomogeneity receiver.
///
dictionary InhomogeneityReceiver : Receiver
{
    Vector wavefront_normal; ///< Normal of the wavefront

    double time_stamp; ///< Time stamp of the inhomogeneity

    double spreading_loss_db; ///< Spreading loss in dB
};

///
/// \brief Inhomogeneity anchor extension for a specular reflection
///
dictionary InhomogeneitySpecularReflection : SpecularReflection
{
    Vector wavefront_normal; ///< Normal of the wavefront

    double time_stamp; ///< Time stamp of the inhomogeneity
};

///
/// \brief Base class for all acoustic materials that can be stored in the \p material_database
///
dictionary MaterialBase
{
};

///
/// \brief Acoustic material with scalar absorption and scattering coefficients
///
dictionary ScalarMaterial : MaterialBase
{
    double absorption; ///< Absorption coefficient, applied to all frequencies
    double scattering; ///< Scattering coefficient, applied to all frequencies
};

///
/// \brief Acoustic material with frequency-dependent absorption and scattering coefficients.
///
/// The absorption and scattering coefficients are stored as magnitude spectra, which are defined for the 1/3 octave band center frequencies.
///
dictionary ThirdOctaveMaterial : MaterialBase
{
    MagnitudeSpectra absorption; ///< Absorption coefficient, defined for the 1/3 octave band center frequencies
    MagnitudeSpectra scattering; ///< Scattering coefficient, defined for the 1/3 octave band center frequencies
    MagnitudeSpectra? impedance_real; ///< Real part of the material impedance, defined for the 1/3 octave band center frequencies, optional
    MagnitudeSpectra? impedance_imag; ///< Imaginary part of the material impedance, defined for the 1/3 octave band center frequencies, optional
};