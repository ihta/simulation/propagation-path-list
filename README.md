# propagation-path-list

This repository contains the definition for the **Propagation Path List** data scheme.
It uses the widely used [JSON](https://www.json.org/json-en.html) format to represent the data.
The scheme stores information about propagation paths between sources and receivers for acoustic propagation.
Please refer to the [documentation](./doc/propagation-path-list.md) for a more detailed description of the scheme.
In addition to the schema definition, implementations for some programming languages are also provided.

Please note that the repository is currently under development, and the content may be subject to minor changes.
An effort will be made to keep the data structures stable, but the documentation and the provided language bindings may be updated.

## Schema

A JSON schema file can be found in the `schema` folder.
This can be used to validate the data scheme.

## Usage

The provided language bindings can be used to read and write propagation path lists in the JSON format.
The defined objects can be used directly, or a translation to other data structures (native to your simulation tool) can be implemented.

## Support and Contribution

If you have any questions or suggestions or want to contribute to the project, please feel free to contact us through the [issue tracker](https://git.rwth-aachen.de/ihta/simulation/propagation-path-list/-/issues) or via e-mail.
You can also checkout our [CONTRIBUTING.md](./CONTRIBUTING.md) file for more information on how to contribute to the project.
We are looking forward to your feedback and contributions.

## Acknowledgment

This data scheme is based on the initial ideas of Jonas Stienen and Armin Erraji.

## Publications

- t.b.d.
