# Contributing Guidelines

Thank you for considering contributing to our project!
We welcome contributions from the community to help improve and grow our project.
Before contributing, please take a moment to review the following guidelines:

## How to Contribute

1. Fork the repository and clone it to your local machine.
2. Create a new branch for your contribution: `git checkout -b feature/my-feature`.
   The branch name should describe the changes you are making.
3. Make your changes and ensure they align with our project's goals and coding standards.
4. Test your changes thoroughly to ensure they work as expected.
5. Commit your changes with clear and descriptive commit messages following the [EU Commit Guidelines](https://ec.europa.eu/component-library/v1.15.0/eu/docs/conventions/git/).
6. Add your changes to the `CHANGELOG.md` file under the `[Unreleased]` section and follow the [Keep a Changelog](https://keepachangelog.com/en/1.1.0/) format.
7. Push your changes to your fork.
8. Open a merge request (MR) against the main branch of our repository.
9. Provide a detailed description of your changes in the MR, including any relevant context or references.
10. Ensure your MR passes all automated checks and tests.
11. Be responsive to feedback and be willing to make further changes if requested.

## Code Style and Standards

- Follow our project's coding style and conventions.
- Write clear, concise, and maintainable code.
- Use meaningful variable names and document your code to improve code readability and usability.

## Reporting Issues

If you encounter a bug or have a feature request, please open an issue on our [repository](https://git.rwth-aachen.de/ihta/simulation/propagation-path-list).
Provide detailed information about the issue, including steps to reproduce it if applicable.

## Code of Conduct

Please adhere to our [project's Code of Conduct](CODE_OF_CONDUCT.md) in all interactions and contributions.

## Licensing

By contributing to our project, you agree to license your contributions under the same license as the project.
Ensure that you have the right to contribute the code under the project's license.

We appreciate your interest in contributing to our project and look forward to your contributions!
If you have any questions or need assistance, feel free to contact us.
