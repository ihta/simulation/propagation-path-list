/**
 * \file propagation_path_list.hpp
 * \brief Automatically generated using poly-scribe-code-gen.

 * \copyright
 * Copyright (c) 2024-present 
 * Distributed under the Apache License 2.0
 */

#pragma once

#include <poly-scribe/poly-scribe.hpp>

// NOLINTBEGIN

namespace propagation_path_list
{
    struct PropagationPathList;
    struct PropagationPath;
    struct PropagationAnchorBase;
    struct Source;
    struct Receiver;
    struct SpecularReflection;
    struct EdgeDiffraction;
    struct Inhomogeneity;
    struct InhomogeneitySource;
    struct InhomogeneityReceiver;
    struct InhomogeneitySpecularReflection;
    struct MaterialBase;
    struct ScalarMaterial;
    struct ThirdOctaveMaterial;

    using Vector = std::array<double, 3>;
    using Quaternion = std::array<double, 4>;
    using MagnitudeSpectra = std::array<double, 31>;


    ///
    /// \brief Top-level datastructure for the propagation path list
    ///
    /// The propagation path list contains a list of propagation paths, which in turn contain a list of propagation anchors.
    /// The propagation anchors can be of type
    /// - Source
    /// - Receiver
    /// - SpecularReflection
    /// - EdgeDiffraction
    /// - Inhomogeneity
    /// - InhomogeneitySource
    /// - InhomogeneityReceiver
    /// - InhomogeneitySpecularReflection
    ///
    /// The propagation path list also contains a material database, which is a dictionary of materials that can be referenced by the propagation anchors.
    /// The material database is optional, and can be used to store materials that are used by the propagation anchors.
    /// The material database is a dictionary of materials, where the key is the material identifier and the value is the material itself.
    ///
    struct PropagationPathList 
    {
        
        std::vector<PropagationPath> propagation_paths ;  ///< List of propagation paths

        std::unordered_map<std::string, std::shared_ptr<MaterialBase>> material_database ;  ///< Optional material database storing MaterialBase objects

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            t_archive(
                poly_scribe::make_scribe_wrap( "propagation_paths", propagation_paths),
                poly_scribe::make_scribe_wrap( "material_database", material_database)
            );
        }
    };

    ///
    /// \brief Container of PropagationAnchors representing a propagation path.
    ///
    struct PropagationPath 
    {
        ///
        /// \brief Identifier of the propagation path.
        ///
        /// This could be the name of the path, or a unique identifier.
        /// For example a path with a source and a receiver might be called "S-R" for source to receiver.
        ///
        std::string identifier ;
        ///
        /// \brief List of propagation anchors
        ///
        /// This list contains the propagation anchors that make up the propagation path.
        ///
        std::vector<std::shared_ptr<PropagationAnchorBase>> propagation_anchors ;

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            t_archive(
                poly_scribe::make_scribe_wrap( "identifier", identifier),
                poly_scribe::make_scribe_wrap( "propagation_anchors", propagation_anchors)
            );
        }
    };

    ///
    /// \brief Base class for all propagation anchors
    ///
    struct PropagationAnchorBase 
    {
        virtual ~PropagationAnchorBase( ) = default;
        ///
        /// \brief Interaction point of the propagation anchor.
        ///
        /// This is the point in space where the propagation anchor interacts with the environment.
        /// For example, for a source this could be the point where the sound is emitted, and for a receiver this could be the point where the sound is received.
        ///
        Vector interaction_point ;

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            t_archive(
                poly_scribe::make_scribe_wrap( "interaction_point", interaction_point)
            );
        }
    };

    ///
    /// \brief Source anchor
    ///
    /// The source anchor is a point in space where sound is emitted.
    /// It is assumed to be a point source, i.e. it has no spatial extent.
    ///
    struct Source : public PropagationAnchorBase
    {
        
        std::string name ;  ///< Name of the source
        ///
        /// \brief Identifier of the directivity pattern
        ///
        /// This is the identifier of the directivity pattern that is used by the receiver.
        /// Currently, the directivity pattern is not part of the data model, but it is assumed that the directivity pattern is stored on disk and is loaded separately.
        ///
        /// \todo The directivies might be added to the data model as well.
        ///
        std::string directivity_id ;

        Quaternion orientation ;  ///< Orientation of the source, defined as a quaternion

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<PropagationAnchorBase>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "name", name),
                poly_scribe::make_scribe_wrap( "directivity_id", directivity_id),
                poly_scribe::make_scribe_wrap( "orientation", orientation)
            );
        }
    };

    ///
    /// \brief Receiver anchor
    ///
    /// The receiver anchor is a point in space where sound is received.
    /// It is assumed to be a point receiver, i.e. it has no spatial extent.
    ///
    struct Receiver : public PropagationAnchorBase
    {
        
        std::string name ;  ///< Name of the source
        ///
        /// \brief Identifier of the directivity pattern
        ///
        /// This is the identifier of the directivity pattern that is used by the receiver.
        /// Currently, the directivity pattern is not part of the data model, but it is assumed that the directivity pattern is stored on disk and is loaded separately.
        ///
        /// \todo The directivies might be added to the data model as well.
        ///
        std::string directivity_id ;

        Quaternion orientation ;  ///< Orientation of the source, defined as a quaternion

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<PropagationAnchorBase>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "name", name),
                poly_scribe::make_scribe_wrap( "directivity_id", directivity_id),
                poly_scribe::make_scribe_wrap( "orientation", orientation)
            );
        }
    };

    ///
    /// \brief Specular reflection anchor
    ///
    /// The specular reflection anchor is a point in space where sound is reflected from a surface.
    /// The reflection is assumed to be specular, i.e. the angle of incidence is equal to the angle of reflection.
    ///
    struct SpecularReflection : public PropagationAnchorBase
    {
        
        long polygon_id ;  ///< Identifier of the polygon that is reflected from
        ///
        /// \brief Identifier of the material
        ///
        /// This identifier is used to reference the material in the material database.
        ///
        std::string material_id ;

        Vector face_normal ;  ///< Normal of the polygon face

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<PropagationAnchorBase>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "polygon_id", polygon_id),
                poly_scribe::make_scribe_wrap( "material_id", material_id),
                poly_scribe::make_scribe_wrap( "face_normal", face_normal)
            );
        }
    };

    ///
    /// \brief Edge diffraction anchor
    ///
    /// The edge diffraction anchor is a point in space where sound is diffracted around an edge.
    /// The diffraction is assumed to be caused by a wedge-shaped edge.
    ///
    /// The main wedge face is defined to be the face that is iluminated by the previous propagation anchor.
    ///
    /// \todo: Do we separate outer and inner edge diffraction?
    ///
    struct EdgeDiffraction : public PropagationAnchorBase
    {
        
        long main_wedge_face_id ;  ///< Identifier of the main wedge polygon

        long opposite_wedge_face_id ;  ///< Identifier of the opposite wedge polygon

        Vector vertex_start ;  ///< Start vertex of the wedges edge

        Vector vertex_end ;  ///< End vertex of the wedges edge

        Vector main_wedge_face_normal ;  ///< Normal of the main wedge polygon face

        Vector opposite_wedge_face_normal ;  ///< Normal of the opposite wedge polygon face

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<PropagationAnchorBase>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "main_wedge_face_id", main_wedge_face_id),
                poly_scribe::make_scribe_wrap( "opposite_wedge_face_id", opposite_wedge_face_id),
                poly_scribe::make_scribe_wrap( "vertex_start", vertex_start),
                poly_scribe::make_scribe_wrap( "vertex_end", vertex_end),
                poly_scribe::make_scribe_wrap( "main_wedge_face_normal", main_wedge_face_normal),
                poly_scribe::make_scribe_wrap( "opposite_wedge_face_normal", opposite_wedge_face_normal)
            );
        }
    };

    ///
    /// \brief Inhomogeneity anchor
    ///
    /// This serves both as the base class for all inhomogeneity types and as a generic inhomogeneity anchor.
    ///
    struct Inhomogeneity : public PropagationAnchorBase
    {
        
        Vector wavefront_normal ;  ///< Normal of the wavefront

        double time_stamp ;  ///< Time stamp of the inhomogeneity

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<PropagationAnchorBase>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "wavefront_normal", wavefront_normal),
                poly_scribe::make_scribe_wrap( "time_stamp", time_stamp)
            );
        }
    };

    ///
    /// \brief Inhomogeneity anchor extension for a source
    ///
    struct InhomogeneitySource : public Source
    {
        
        Vector wavefront_normal ;  ///< Normal of the wavefront

        double time_stamp ;  ///< Time stamp of the inhomogeneity

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<Source>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "wavefront_normal", wavefront_normal),
                poly_scribe::make_scribe_wrap( "time_stamp", time_stamp)
            );
        }
    };

    ///
    /// \brief Inhomogeneity anchor extension for a receiver
    ///
    /// Since the distance law does not apply to inhomogenious media, the spreading loss of the path is stored in the inhomogeneity receiver.
    ///
    struct InhomogeneityReceiver : public Receiver
    {
        
        Vector wavefront_normal ;  ///< Normal of the wavefront

        double time_stamp ;  ///< Time stamp of the inhomogeneity

        double spreading_loss_db ;  ///< Spreading loss in dB

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<Receiver>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "wavefront_normal", wavefront_normal),
                poly_scribe::make_scribe_wrap( "time_stamp", time_stamp),
                poly_scribe::make_scribe_wrap( "spreading_loss_db", spreading_loss_db)
            );
        }
    };

    ///
    /// \brief Inhomogeneity anchor extension for a specular reflection
    ///
    struct InhomogeneitySpecularReflection : public SpecularReflection
    {
        
        Vector wavefront_normal ;  ///< Normal of the wavefront

        double time_stamp ;  ///< Time stamp of the inhomogeneity

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<SpecularReflection>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "wavefront_normal", wavefront_normal),
                poly_scribe::make_scribe_wrap( "time_stamp", time_stamp)
            );
        }
    };

    ///
    /// \brief Base class for all acoustic materials that can be stored in the \p material_database
    ///
    struct MaterialBase 
    {
        virtual ~MaterialBase( ) = default;
        
        template <class Archive>
        void serialize (Archive& t_archive)
        {
        }
    };

    ///
    /// \brief Acoustic material with scalar absorption and scattering coefficients
    ///
    struct ScalarMaterial : public MaterialBase
    {
        
        double absorption ;  ///< Absorption coefficient, defined for the 1/3 octave band center frequencies

        double scattering ;  ///< Scattering coefficient, defined for the 1/3 octave band center frequencies

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<MaterialBase>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "absorption", absorption),
                poly_scribe::make_scribe_wrap( "scattering", scattering)
            );
        }
    };

    ///
    /// \brief Acoustic material with frequency-dependent absorption and scattering coefficients.
    ///
    /// The absorption and scattering coefficients are stored as magnitude spectra, which are defined for the 1/3 octave band center frequencies.
    ///
    struct ThirdOctaveMaterial : public MaterialBase
    {
        
        MagnitudeSpectra absorption ;  ///< Absorption coefficient, defined for the 1/3 octave band center frequencies

        MagnitudeSpectra scattering ;  ///< Scattering coefficient, defined for the 1/3 octave band center frequencies

        MagnitudeSpectra impedance_real ;  ///< Real part of the material impedance, defined for the 1/3 octave band center frequencies, optional

        MagnitudeSpectra impedance_imag ;  ///< Imaginary part of the material impedance, defined for the 1/3 octave band center frequencies, optional

        template <class Archive>
        void serialize (Archive& t_archive)
        {
            cereal::base_class<MaterialBase>( this ).base_ptr->serialize( t_archive );
            t_archive(
                poly_scribe::make_scribe_wrap( "absorption", absorption),
                poly_scribe::make_scribe_wrap( "scattering", scattering),
                poly_scribe::make_scribe_wrap( "impedance_real", impedance_real),
                poly_scribe::make_scribe_wrap( "impedance_imag", impedance_imag)
            );
        }
    };
}

POLY_SCRIBE_REGISTER_TYPE_WITH_NAME( propagation_path_list::Source, "Source" );
POLY_SCRIBE_REGISTER_TYPE_WITH_NAME( propagation_path_list::Receiver, "Receiver" );
POLY_SCRIBE_REGISTER_TYPE_WITH_NAME( propagation_path_list::SpecularReflection, "SpecularReflection" );
POLY_SCRIBE_REGISTER_TYPE_WITH_NAME( propagation_path_list::EdgeDiffraction, "EdgeDiffraction" );
POLY_SCRIBE_REGISTER_TYPE_WITH_NAME( propagation_path_list::Inhomogeneity, "Inhomogeneity" );
POLY_SCRIBE_REGISTER_TYPE_WITH_NAME( propagation_path_list::ScalarMaterial, "ScalarMaterial" );
POLY_SCRIBE_REGISTER_TYPE_WITH_NAME( propagation_path_list::ThirdOctaveMaterial, "ThirdOctaveMaterial" );

// NOLINTEND