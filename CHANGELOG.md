# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- format specification for C++, Python and CMake
- `CONTRIBUTION.md` and `CODE_OF_CONDUCT.md` file
- `CHANGELOG.md` file
- Python implementation of the data scheme
- JSON schema for the data scheme
- C++ implementation of the data scheme
- data scheme IDL description with documentation
- `CITATION.cff` file
- Initial repository setup
