from typing import Annotated, Dict, List, Literal, Union

from annotated_types import Len
from pydantic import BaseModel, Field

Vector = Annotated[List[float], Len(min_length=3, max_length=3)]
Quaternion = Annotated[List[float], Len(min_length=4, max_length=4)]
MagnitudeSpectra = Annotated[List[float], Len(min_length=31, max_length=31)]


class PropagationAnchorBase(BaseModel):
    interaction_point: Vector


class Inhomogeneity(PropagationAnchorBase):
    wavefront_normal: Vector
    time_stamp: float
    type: Literal["Inhomogeneity"] = "Inhomogeneity"


class EdgeDiffraction(PropagationAnchorBase):
    main_wedge_face_id: int
    opposite_wedge_face_id: int
    vertex_start: Vector
    vertex_end: Vector
    main_wedge_face_normal: Vector
    opposite_wedge_face_normal: Vector
    type: Literal["EdgeDiffraction"] = "EdgeDiffraction"


class SpecularReflection(PropagationAnchorBase):
    polygon_id: int
    material_id: str
    face_normal: Vector
    type: Literal["SpecularReflection"] = "SpecularReflection"


class InhomogeneitySpecularReflection(SpecularReflection):
    wavefront_normal: Vector
    time_stamp: float
    type: Literal["InhomogeneitySpecularReflection"] = "InhomogeneitySpecularReflection"


class Receiver(PropagationAnchorBase):
    name: str
    directivity_id: str
    orientation: Quaternion
    type: Literal["Receiver"] = "Receiver"


class InhomogeneityReceiver(Receiver):
    wavefront_normal: Vector
    time_stamp: float
    spreading_loss_db: float
    type: Literal["InhomogeneityReceiver"] = "InhomogeneityReceiver"


class Source(PropagationAnchorBase):
    name: str
    directivity_id: str
    orientation: Quaternion
    type: Literal["Source"] = "Source"


class InhomogeneitySource(Source):
    wavefront_normal: Vector
    time_stamp: float
    type: Literal["InhomogeneitySource"] = "InhomogeneitySource"


class PropagationPath(BaseModel):
    identifier: str
    propagation_anchors: Annotated[
        List[
            Union[
                Source,
                Receiver,
                SpecularReflection,
                EdgeDiffraction,
                Inhomogeneity,
                InhomogeneitySource,
                InhomogeneityReceiver,
                InhomogeneitySpecularReflection,
                PropagationAnchorBase,
            ]
        ],
        Field(discriminator="type"),
    ]


class MaterialBase(BaseModel):
    pass


class ThirdOctaveMaterial(MaterialBase):
    absorption: MagnitudeSpectra
    scattering: MagnitudeSpectra
    impedance_real: MagnitudeSpectra
    impedance_imag: MagnitudeSpectra
    type: Literal["ThirdOctaveMaterial"] = "ThirdOctaveMaterial"


class ScalarMaterial(MaterialBase):
    absorption: float
    scattering: float
    type: Literal["ScalarMaterial"] = "ScalarMaterial"


class PropagationPathList(BaseModel):
    propagation_paths: List[PropagationPath]
    material_database: Annotated[
        Dict[str, Union[ScalarMaterial, ThirdOctaveMaterial, MaterialBase]],
        Field(discriminator="type"),
    ]
